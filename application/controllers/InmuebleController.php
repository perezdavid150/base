<?php

    class InmuebleController extends CI_Controller{
        public function __construct() {
            parent::__construct();
            $this->load->helper("url"); 
            $this->load->model("InmuebleModel");
            $this->load->library("session");
        }
     
        public function index(){
            $Inmuebles["ver"]=$this->InmuebleModel->ver();
			$Inmuebles["ciudades"]=$this->InmuebleModel->verCiudades();
			$Inmuebles["transaccion"]=$this->InmuebleModel->verTransacciones();
            $this->load->view("InmuebleView",$Inmuebles);
        }
     
        public function add(){
            if($this->input->post("CiudadId")){                
                $add=$this->InmuebleModel->add(
                    $this->input->post("CiudadId"),
                    $this->input->post("Direccion"),
                    $this->input->post("TransaccionId"),
                    $this->input->post("NumHabitaciones"),
                    $this->input->post("NumBanos"),
                    $this->input->post("NumParqueaderos"),
                    $this->input->post("Antiguedad"),
                    $this->input->post("Area")
                );
            }
        }
        
        public function update($InmuebleId){
              if($this->input->post("CiudadId")){
                    $update=$this->InmuebleModel->update(
                            $InmuebleId,
                            "modificar",
                            $this->input->post("CiudadId"),
                            $this->input->post("Direccion"),
                            $this->input->post("TransaccionId"),
                            $this->input->post("NumHabitaciones"),
                            $this->input->post("NumBanos"),
                            $this->input->post("NumParqueaderos"),
                            $this->input->post("Antiguedad"),
                            $this->input->post("Area")
                            );
                    if($update){
                        $msg['msg'] = true;
                        echo json_encode ($msg);
                    }else{
                        $msg['msg'] = false;
                        echo json_encode ($msg);
                    }
                }            
        }

        public function delete($InmuebleId){
            
              $eliminar=$this->InmuebleModel->delete($InmuebleId);
              if($eliminar==true){
				  $msg['msg'] = true;
                  echo json_encode ($msg);
              }else{
				  $msg['msg'] = false;
                  echo json_encode ($msg);
              }
              return true;
        }
        
        
        public function cargarCiudad($ciudad){
            $ciudad = $this->InmuebleModel->cargarCiudad($ciudad);
            $html = '';
            foreach($ciudad as $cd){
                $html .= '<option value="'.$cd->CiudadId.'">'.$cd->NombreCiudad.'</option>';
            }
            $html .= '';
            echo json_encode($html);
            die;
        }
        
    }


?>