<?php

    class InmuebleModel extends CI_Model{
        public function __construct() {
            parent::__construct();
            $this->load->database();
        }
        
        public function ver(){
            $consulta=$this->db->query("SELECT * FROM inmueble i 
										INNER JOIN ciudad c ON c.CiudadId = i.CiudadId and c.activo = 1 
										INNER JOIN transaccion t ON t.TransaccionId = i.TransaccionId and t.activo = 1 
										WHERE i.Activo = 1");
            return $consulta->result();
        }
		
		public function verCiudades(){
            $consulta=$this->db->query("SELECT * FROM ciudad WHERE activo = 1");
            return $consulta->result();
        }
		
		public function verTransacciones(){
            $consulta=$this->db->query("SELECT * FROM transaccion WHERE activo = 1");
            return $consulta->result();
        }
     
        public function add($CiudadId,$Direccion,$TransaccionId,$NumHabitaciones,$NumBanos,$NumParqueaderos,$Antiguedad,$Area){
                $consulta=$this->db->query("INSERT INTO inmueble 
											VALUES
											(NULL,'$CiudadId','$Direccion','$TransaccionId','$NumHabitaciones','$NumBanos','$NumParqueaderos','$Antiguedad','$Area',1);");
                if($consulta==true){
                  return true;
                }else{
                    return false;
                }
        }
     
        public function update($InmuebleId,$modificar="NULL",$CiudadId="NULL",$Direccion="NULL",$TransaccionId="NULL",$NumHabitaciones="NULL",$NumBanos="NULL",$NumParqueaderos="NULL",$Antiguedad="NULL",$Area="NULL"){
            if($modificar=="NULL"){
                $consulta=$this->db->query("SELECT * FROM Inmueble WHERE InmuebleId=$InmuebleId");
                return $consulta->result();
            }else{
              $consulta=$this->db->query("
                  UPDATE Inmueble SET CiudadId='$CiudadId', Direccion='$Direccion',
                  TransaccionId='$TransaccionId', NumHabitaciones='$NumHabitaciones',
				  NumBanos='$NumBanos', NumParqueaderos='$NumParqueaderos', Antiguedad='$Antiguedad', Area='$Area' WHERE InmuebleId=$InmuebleId;
                      ");
              if($consulta==true){
                  return true;
              }else{
                  return false;
              }
            }
        }
     
        public function delete($InmuebleId){
           $consulta=$this->db->query("UPDATE inmueble SET activo = 0 WHERE InmuebleId=$InmuebleId");
           if($consulta==true){
               return true;
           }else{
               return false;
           }
        }

    }

?>