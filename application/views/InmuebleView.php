<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>Prueba Inmuebles</title>
        <script type="text/javascript" src="assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script>
            
            function update(update){
                    var update = update;
                    var CiudadId = $('#CiudadId' + update).val();
                    var Direccion = $('#Direccion' + update).val();
                    var TransaccionId = $('#TransaccionId' + update).val();
                    var NumHabitaciones = $('#NumHabitaciones' + update).val();
                    var NumBanos = $('#NumBanos' + update).val();
                    var NumParqueaderos = $('#NumParqueaderos' + update).val();
                    var Antiguedad = $('#Antiguedad' + update).val();
                    var Area = $('#Area' + update).val();
                    $.ajax({
                        method: "POST",
                        url: "<?= base_url("InmuebleController/update") ?>/" + update,
                        data: {
                            update: update,
                            CiudadId: CiudadId,
                            Direccion: Direccion,
                            TransaccionId: TransaccionId,
                            NumHabitaciones: NumHabitaciones,
                            NumBanos: NumBanos,
                            NumParqueaderos: NumParqueaderos,
                            Antiguedad: Antiguedad,
                            Area: Area},
                        dataType: "json",
                        success: function (data) {
                            if (data.msg == true) {
                                alert('Actualizado correctamente');
                            } else {
                                alert('Ocurrio un error al actualizar el inmueble');
                            }
                        },
                        error: function (e) {
                            alert(e);
                        }
                    });

                };
                
                
                function eliminar(eliminar){
                var eliminar = eliminar;
                    $.ajax({
                        method: "POST",
                        url: "<?= base_url("InmuebleController/delete"); ?>/" + eliminar,
                        data: {action: "delete", eliminar: eliminar},
                        dataType: "json",
                        success: function (data) {
                            if (data.msg == true) {
                                $('#tr' + eliminar).html('');
                                alert('Inmueble eliminado correctamente');
                            } else {
                                alert('Ocurrio un error al eliminar el inmueble');
                            }
                        },
                        error: function (e) {
                            alert(e);
                        }
                    });
                }
            $(document).ready(function () {

                $('label').on("dblclick", function () {
                    $(this).attr("style", "display:none");
                    var dataSet = $(this).attr("data-set");
                    $('#' + dataSet).attr("style", "display:block");
                });



                $('form#formularioAdd').submit(function (e) {
                    var form = $(this);
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url("InmuebleController/add"); ?>",
                        data: form.serialize(),
                        dataType: "html",
                        success: function () {
                            alert("Inmueble añadido correctamente");
                            location.reload(true);
                        },
                        error: function () {
                            alert("Error añadiendo inmueble.");
                        }
                    });
                });

            });
        </script>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    </head>
    <body>
        <h2>Prueba Inmuebles</h2>
        <p>&nbsp;</p>

        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
            Añadir nuevo
        </button>
        <p>&nbsp;</p>

        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Nota:</strong> Para editar, de doble click en el elemento a editar. Una vez editado de click en modificar para guardar los cambios.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Ciudad</th>
                    <th scope="col">Direccion</th>
                    <th scope="col">Transaccion</th>
                    <th scope="col">#Habitaciones</th>
                    <th scope="col">#Baños</th>
                    <th scope="col">#Parqueaderos</th>
                    <th scope="col">Antiguedad</th>
                    <th scope="col">Area</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($ver as $fila) {
                    ?>
                    <tr id="tr<?= $fila->InmuebleId; ?>">
                        <td>
                            <select id="CiudadId<?= $fila->InmuebleId; ?>" style="display:none">
                                <?php foreach ($ciudades as $filaC) { ?>
                                    <option <?php echo ($filaC->CiudadId == $fila->CiudadId) ? 'selected' : ''; ?> value="<?php echo $filaC->CiudadId; ?>"><?php echo $filaC->NombreCiudad; ?></option>
                                <?php } ?>
                            </select>
                            <label data-set="CiudadId<?= $fila->InmuebleId; ?>"><?= $fila->NombreCiudad; ?></label>
                        </td>
                        <td>
                            <input type="text" id="Direccion<?= $fila->InmuebleId; ?>" style="display:none" value="<?= $fila->Direccion; ?>" />
                            <label data-set="Direccion<?= $fila->InmuebleId; ?>"><?= $fila->Direccion; ?></label>
                        </td>
                        <td>
                            <select id="TransaccionId<?= $fila->InmuebleId; ?>" style="display:none">
                                <?php foreach ($transaccion as $filaT) { ?>
                                    <option <?php echo ($filaT->TransaccionId == $fila->TransaccionId) ? 'selected' : ''; ?> value="<?php echo $filaT->TransaccionId; ?>"><?php echo $filaT->NombreTransaccion; ?></option>
                                <?php } ?>
                            </select>
                            <label data-set="TransaccionId<?= $fila->InmuebleId; ?>"><?= $fila->NombreTransaccion; ?></label>
                        </td>
                        <td>
                            <select id="NumHabitaciones<?= $fila->InmuebleId; ?>" style="display:none" name="NumHabitaciones">
                                <option selected>Seleccione...</option>
                                <?php
                                $i = 1;
                                while ($i <= 4) {
                                    ?>
                                    <option <?php echo ($fila->NumHabitaciones == $i) ? 'selected' : ''; ?> value="<?= $i ?>"><?= $i ?></option>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </select>
                            <label data-set="NumHabitaciones<?= $fila->InmuebleId; ?>"><?= $fila->NumHabitaciones; ?></label>
                        </td>
                        <td>
                            <select id="NumBanos<?= $fila->InmuebleId; ?>" style="display:none" name="NumBanos">
                                <option selected>Seleccione...</option>
                                <?php
                                $i = 1;
                                while ($i <= 4) {
                                    ?>
                                    <option <?php echo ($fila->NumBanos == $i) ? 'selected' : ''; ?> value="<?= $i ?>"><?= $i ?></option>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </select>
                            <label data-set="NumBanos<?= $fila->InmuebleId; ?>"><?= $fila->NumBanos; ?></label>
                        </td>
                        <td>
                            <select id="NumParqueaderos<?= $fila->InmuebleId; ?>" style="display:none" name="NumParqueaderos">
                                <option selected>Seleccione...</option>
                                <?php
                                $i = 1;
                                while ($i <= 4) {
                                    ?>
                                    <option <?php echo ($fila->NumParqueaderos == $i) ? 'selected' : ''; ?> value="<?= $i ?>"><?= $i ?></option>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </select>
                            <label data-set="NumParqueaderos<?= $fila->InmuebleId; ?>"><?= $fila->NumParqueaderos; ?></label>
                        </td>
                        <td>
                            <input type="text" id="Antiguedad<?= $fila->InmuebleId; ?>" style="display:none" value="<?= $fila->Antiguedad; ?>" />
                            <label data-set="Antiguedad<?= $fila->InmuebleId; ?>"><?= $fila->Antiguedad; ?></label>
                        </td>
                        <td>
                            <input type="text" id="Area<?= $fila->InmuebleId; ?>" style="display:none" value="<?= $fila->Area; ?>" />
                            <label data-set="Area<?= $fila->InmuebleId; ?>"><?= $fila->Area; ?> m2</label>
                        </td>
                        <td>
                            <a href="#" data-id="<?= $fila->InmuebleId; ?>" id="update" onclick="update(<?= $fila->InmuebleId; ?>)" class="btn btn-warning">Modificar</a>
                            <a href="#" data-id="<?= $fila->InmuebleId; ?>" id="eliminar" onclick="eliminar(<?= $fila->InmuebleId; ?>)" class="btn btn-danger">Eliminar</a>                        
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>



        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nuevo Inmueble</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">




                        <form id="formularioAdd" action="#" method="post">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Ciudad</label>
                                    <select id="CiudadId" name="CiudadId" class="form-control">
                                        <option value="0">Seleccione...</option>
                                        <?php foreach ($ciudades as $fila) { ?>
                                            <option value="<?php echo $fila->CiudadId; ?>"><?php echo $fila->NombreCiudad; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputAddress">Transacción</label>
                                    <select id="TransaccionId" name="TransaccionId" class="form-control">
                                        <option value="0">Seleccione...</option>
                                        <?php foreach ($transaccion as $fila) { ?>
                                            <option value="<?php echo $fila->TransaccionId; ?>"><?php echo $fila->NombreTransaccion; ?></option>
                                        <?php } ?>
                                    </select>                                
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword4">Dirección Inmueble</label>
                                <input type="text" name="Direccion" class="form-control" id="Direccion" placeholder="Direccion">
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="inputCity"># Habitaciones</label>
                                    <select id="NumHabitaciones" name="NumHabitaciones" class="form-control">
                                        <option selected>Seleccione...</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputCity"># Baños</label>
                                    <select id="NumBanos" name="NumBanos" class="form-control">
                                        <option selected>Seleccione...</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="inputCity"># Parqueaderos</label>
                                    <select id="NumParqueaderos" name="NumParqueaderos" class="form-control">
                                        <option selected>Seleccione...</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputState"># Antiguedad</label>
                                    <input type="text" class="form-control" id="Antiguedad" name="Antiguedad">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputState">Area</label>
                                    <input type="text" class="form-control" id="Area" name="Area" placeholder="Area">
                                </div>
                            </div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <input type="submit" id="Add" class="btn btn-primary" value="Guardar" />
                        </form>





                    </div>
                </div>
            </div>
        </div>


    </body>
</html>