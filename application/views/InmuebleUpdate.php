<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="UTF-8" />
        <title>Modificar Inmuebles</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        
    </head>
    <body>
        <h2>Modificar Inmuebles</h2>
		<table>
		<?php foreach ($update as $fila){ ?>
        <form action="<?=base_url("InmuebleController/update/$fila->InmuebleId");?>" method="POST">
            
			<tr>
			<td>Ciudad</td>
            <td><select name="CiudadId">
				   <?php foreach($ciudades as $filac){ ?>
						<option value="<?php echo $filac->CiudadId; ?>"><?php echo $filac->NombreCiudad; ?></option>
				   <?php } ?>
				   </select></td>
            </tr>
			<tr>
			<td>Direccion</td>
			<td><input type="text"  name="Direccion" value="<?=$fila->Direccion?>"/></td>
			</tr>
			<tr>
			<td>Transaccion</td>
            <td><select name="TransaccionId">
				   <?php foreach($transaccion as $filat){ ?>
						<option value="<?php echo $filat->TransaccionId; ?>"><?php echo $filat->NombreTransaccion; ?></option>
				   <?php } ?>
				   </select></td>
			</tr>
			<tr>
			<td># Habitaciones</td>
            <td><input type="apellido" name="NumHabitaciones" value="<?=$fila->NumHabitaciones?>"/></td>
			</tr>
			<tr>
			<td># Baños</td>
            <td><input type="apellido" name="NumBanos" value="<?=$fila->NumBanos?>"/></td>
			</tr>
			<tr>
			<td># Baños</td>
            <td><input type="apellido" name="NumParqueaderos" value="<?=$fila->NumParqueaderos?>"/></td>
			</tr>
			<tr>
			<td>Antiguedad</td>
            <td><input type="apellido" name="Antiguedad" value="<?=$fila->Antiguedad?>"/></td>
			</tr>
			<tr>
			<td>Area</td>
            <td><input type="apellido" name="Area" value="<?=$fila->Area?>"/></td>
			<tr>
            <td><input type="submit" name="submit" value="Modificar" class="btn btn-primary"/></td>
			</tr>
            <?php } ?>
        </form>
		<table>
        <a href="<?=base_url()?>">Volver</a>
    </body>
</html>
